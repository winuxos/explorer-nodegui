const { QMainWindow, QWidget, QLabel, QPushButton, QSvgWidget, QPixmap, QGridLayout, QGraphicsColorizeEffect, QSize, QLineEdit } = require('@nodegui/nodegui');
const resolution = require('screen-resolution');
const fs = require('fs');
const os = require('os');

const d = { // defaults
    tb: { // Taskbar
        i: new QSize(40, 48), // icon dimensions
    }
};

function getSvg(name, width, height) {
    const svg = new QSvgWidget();
    fs.readFile(`/usr/share/svgs/${name}.svg`, (err, buffer) => {
        if (!err) svg.load(buffer);
    })

    // svg.load(`/usr/share/svgs/${name}.svg`);
    svg.setObjectName(name);
    svg.setMinimumSize(width, height);
    return svg;
}
// const { width, height } = require('screenz');
// console.log(screen.width,screen.height);

var res;
function catchErr(err, file) { 
    if (err) {
        console.error('Error caught:', err);
    }
}
const taskbar = new QWidget();
const desktop = new QMainWindow();
const background = new QLabel();
// const backgroundImage = new QPixmap(settings.background); // be sure that if it is reenabled, move it inside scope of settings var.
const startMenu = new QPushButton();
const searchbar = new QLineEdit();
const cortanaBtn = new QPushButton();
const taskviewBtn = new QPushButton();
const appview = new QWidget();
const actionitems = new QWidget();
resolution.get().then(result => {
    res = result;
    console.log('Screen resolution:', res);
    const h = os.homedir();
    fs.readFile(h + '/.config/windtu/settings.json', (err, data) => {
        if (err) {
            data = JSON.stringify({
                background: "/usr/share/backgrounds/img0.jpg"
            });
            fs.mkdir(h + '/.config/windtu/', { recursive: true }, catchErr);
            fs.writeFile(h + '/.config/windtu/settings.json', data, catchErr);
        }
        console.log('Retrieved settings');
        var settings = JSON.parse(data);
        /** Taskbar **/
        console.log('Creating Taskbar...');
        taskbar.setLayout(new QGridLayout);
        console.log('Stored width, height', d.tb.i.width(), d.tb.i.height());
        taskbar.layout.setRowMinimumHeight(0, d.tb.i.height());
        // taskbar.layout.setOriginCorner(0);
        taskbar.setGeometry(0, res.height - d.tb.i.height(), res.width, d.tb.i.height());
        taskbar.layout.setHorizontalSpacing(1);
        // taskbar.setWindowFlag(2048, true);
        // taskbar.setWindowFlag(262144, true);
        taskbar.setAttribute(105, true);
        function posRightOf(widget) {
            return widget.pos().x + widget.size().width();
        }

        /** Start Menu **/
        console.log('Creating Start Menu');
        taskbar.layout.setColumnMinimumWidth(0, d.tb.i.width());
        startMenu.setFlat(true);
        const winIcon = getSvg("microsoft-windows");
        winIcon.setGeometry(0, 0, d.tb.i.width(), d.tb.i.height());
        // winIcon.setInlineStyle('fill: white');
        startMenu.setIcon(winIcon);
        startMenu.setIconSize(d.tb.i);
        startMenu.resize(d.tb.i.width(), d.tb.i.height());
        // startMenu.setGeometry(0, 0, d.tb.i.width(), d.tb.i.height());
        taskbar.layout.addWidget(startMenu, 0, 0);

        /** Search Bar **/
        console.log('Creating Search Bar');
        taskbar.layout.setColumnMinimumWidth(1, d.tb.i.width());
        searchbar.resize(d.tb.i.width() * 8.6, d.tb.i.height());
        // searchbar.setGeometry(posRightOf(startMenu), 0, d.tb.i.height() * 8.6, d.tb.i.height());
        searchbar.setPlaceholderText('Search...');
        taskbar.layout.addWidget(searchbar, 0, 1);

        /** Cortana Button **/
        console.log('Creating Cortana Button');
        taskbar.layout.setColumnMinimumWidth(2, d.tb.i.width());
        cortanaBtn.setFlat(true);
        cortanaBtn.resize(d.tb.i.width(), d.tb.i.height());
        // cortanaBtn.setGeometry(posRightOf(searchbar), 0, d.tb.i.width(), d.tb.i.height());
        taskbar.layout.addWidget(cortanaBtn, 0, 2);

        /** Taskview Button **/
        console.log('Creating Taskview Button');
        taskbar.layout.setColumnMinimumWidth(3, d.tb.i.width());
        taskviewBtn.setFlat(true);
        taskviewBtn.resize(d.tb.i.width(), d.tb.i.height());
        // taskviewBtn.setGeometry(posRightOf(cortanaBtn), 0, d.tb.i.width(), d.tb.i.height());
        taskbar.layout.addWidget(taskviewBtn, 0, 3);

        /** Action Items **/
        console.log('Creating Action Items');
        taskbar.layout.setColumnMinimumWidth(5, d.tb.i.width());
        // actionitems.setGeometry(res.width - (d.tb.i.width() * 6.25), 0, d.tb.i.width() * 6.25, d.tb.i.height());
        taskbar.layout.addWidget(actionitems, 0, 5);

        /** App View **/
        console.log('Creating App View');
        taskbar.layout.setColumnMinimumWidth(4, d.tb.i.width());
        // appview.setGeometry(posRightOf(taskviewBtn), 0, res.width - posRightOf(appview), d.tb.i.height());
        taskbar.layout.addWidget(appview, 0, 4);
        taskbar.layout.setColumnStretch(4,1);
        taskbar.show();

        /** Desktop **/
        console.log('Creating Desktop...');
        desktop.setWindowFlag(17, true);
        desktop.setAttribute(110, true);
        desktop.setAttribute(93, true);
        desktop.setGeometry(0, 0, res.width, res.height);
        desktop.setStyleSheet(`background-color: black; background-image: url(${settings.background}; background-position: center; )`);
        /*
        backgroundImage.scaled(res.width, res.height, 2);
        const palette = new QPalette();
        palette.setBrush(QPalette.WindowText, backgroundImage);
        desktop.setPalette(palette);
        // Alt
        desktop.layout.addWidget(background);
        backgroundImage.scaled(res.width, res.height, 2);
        background.setPixmap(backgroundImage);
        desktop.setCentralWidget(background);
         */
        desktop.show();
    });
}).catch(catchErr);
global.taskbar = taskbar;
global.desktop = desktop;
